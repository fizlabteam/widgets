#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDialog>
#include <QPainter>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    //Инициализация интерфейса (автоматически)
    ui->setupUi(this);

    //Так можно менять текст элементов управления
    ui->label->setText("Привет мир!");  //Метка
    ui->pushButton->setText("Кнопка");  //Кнопка
    ui->lineEdit->setText("Hello!");    //Текстовое поле

    //Так можно менять положение и размер элементов
    ui->label->setGeometry(10,10,100,50);

    //Соединение сигнала со слотом вручную
    //(обработка нажатия кнопки)
    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(buttonClick()));

    //Так можно рисовать прямо в окне
    //создаем изображение
    QPixmap pixmap(100,100);
    //Объект класса-рисовальщика
    QPainter painter(&pixmap);
    //Заливка прямоугольной области
    //если задать размер изображения,
    //то, по сути, можно менять цвет фона
    painter.fillRect(0,0,100,100,QBrush(QColor(255,255,255)));
    //Меняем стандартный цвет отрисовки контуров
    painter.setPen(Qt::red);
    //Рисуем линию
    painter.drawLine(0,0,100,100);
    //Рисуем точку
    painter.drawPoint(10,5);
    //Рисуем окружность (как эллипс)
    painter.drawEllipse(QPoint(50,50),6,6);
    //Линия посередине
    painter.drawLine(pixmap.width()/2,0,pixmap.width()/2,pixmap.height());
    //Так можно выводить текст
    painter.drawText(10,10,"Text");
    //После того, как закончили рисовать,
    //присваиваем метке №3 полученное изображение
    ui->label_3->setPixmap(pixmap);

    //Графики
    //получаем указатель на виджет окна,
    //в котором будем выводить графики
    QCustomPlot* customPlot = ui->plot;
    //Инициализируем векторы с данными
    QVector<double> x(101), y(101), y1(101);
    //заполняем данными
    for (int i=0;i<101;++i)
    {
        x[i] = i/50.0 - 1;
        y[i] = 2*x[i]*x[i];
        y1[i] = x[i]*x[i];
    }
    //Добавление графика на виджет
    customPlot->addGraph();
    //теперь у нас есть график с индексом 0
    //задаем ему данные
    customPlot->graph(0)->setData(x,y);
    //Так можно явно задать границы областей вывода
    //customPlot->xAxis->setRange(-1,1);
    //customPlot->yAxis->setRange(0,1);

    //А так можно сделать это автоматически
    customPlot->graph(0)->rescaleAxes();

    //Добавляем втором график, красного цвета
    customPlot->addGraph();
    customPlot->graph(1)->setPen(QPen(Qt::red));
    customPlot->graph(1)->setData(x,y1);

    //Эта функция добавляет 3 взаимодействия с виджетом:
    //1) Можно перетаскивать график, меняя области вывода
    //2) Можно делать масштабирование с помощью колесика мышки
    //3) Можно выбирать графики
    customPlot->setInteractions(QCP::iRangeDrag |
                                QCP::iRangeZoom |
                                QCP::iSelectPlottables);
}

//Слот-обработчик нажатия на первую кнопку
void MainWindow::buttonClick()
{
    //Считываем строку из текстовогоо поля
    QString text = ui->lineEdit->text();
    //и закидываем ее в первую метку
    ui->label->setText(text);

    //А так можно создать еще одно окно
    QDialog *qd = new QDialog();
    qd->show();
}

//Деструктор: очищает интерфейс
MainWindow::~MainWindow()
{
    delete ui;
}

//А этот слот создался автоматом из дизайнера
void MainWindow::on_pushButton_2_clicked()
{
    //Берем значения из левого и правого полей ввода
    QString val1 = ui->lineEdit->text();
    QString val2 = ui->lineEdit_2->text();
    //Конвертируем в целые числа и считаем сумму
    int num = val1.toInt() + val2.toInt();
    //Преобразуем результат в строку и записываем в первую метку
    ui->label->setText(QString::number(num));
}






